package com.example.demo.constant;

public class ResponseMessage {
    public static final String SUCCESS = "success";
    public static final String INTERNAL_ERROR = "internal server error";
    public static final String INVALID_FIELDS = "invalid fields";

    public static final String USER_NOT_FOUND = "user not found";
    public static final String USERNAME_EXIST = "username exist";
    public static final String WRONG_CLIENT_ID_OR_SECRET = "wrong client id or secret";
    public static final String WRONG_USERNAME_OR_PASSWORD = "wrong username or password";
    public static final String WRONG_PASSWORD = "wrong password";

    public static final String ACCESS_TOKEN_EXPIRED = "Access token expired";
    public static final String INVALID_OR_EXPIRED_REFRESH_TOKEN = "invalid or expired refresh token";

    public static final String ROLE_IS_NOT_ALLOWED = "your role is not allowed for this api";
    public static final String USER_BANNED = "user banned";
    public static final String ROLE_NOT_FOUND = "role not found";
    public static final String ROLE_EXIST = "role exist";

    public static final String USER_IS_NOT_STAFF = "user is not staff";
    public static final String USER_IS_NOT_STUDENT = "user is not student";

    public static final String DEPARTMENT_NOT_FOUND = "department not found";
    public static final String NATIONALITY_NOT_FOUND = "nationality not found";
    public static final String PRIORITY_TARGET_NOT_FOUND = "priority target not found";
    public static final String RACE_NOT_FOUND = "race not found";
    public static final String RELIGION_NOT_FOUND = "religion not found";
    public static final String TALENT_NOT_FOUND = "talent not found";
    public static final String REGION_NOT_FOUND = "region not found";
    public static final String DISTRICT_NOT_FOUND = "district not found";
    public static final String WARD_NOT_FOUND = "ward not found";
    public static final String SCHOOL_NOT_FOUND = "school not found";
    public static final String MAJOR_NOT_FOUND = "major not found";
    public static final String FAMILY_ROLE_NOT_FOUND = "family role not found";
    public static final String STUDENT_NOT_FOUND = "student not found";
    public static final String ROOM_FEE_NOT_FOUND = "room fee not found";
    public static final String BUILDING_NOT_FOUND = "building not found";
    public static final String ADVERTISEMENT_NOT_FOUND = "advertisement not found";
    public static final String ANNOUNCEMENT_NOT_FOUND =  "announcement not found";
    public static final String ANNOUNCEMENT_TYPE_NOT_FOUND = "announcement type not found";
    public static final String ROOM_TYPE_NOT_FOUND = "room type not found";

    public static final String MAJOR_NOT_FOUND_IN_SCHOOL = "major not found in school";
    public static final String MAJOR_EXIST_IN_SCHOOL = "major exist in school";
    public static final String CLASS_NOT_FOUND_IN_MAJOR_OF_SCHOOL = "class not found in major of school";
    public static final String DISTRICT_NOT_FOUND_IN_REGION = "district not found in region";
    public static final String WARD_NOT_FOUND_IN_DISTRICT = "district not found in district";
    public static final String ROOM_NOT_FOUND_IN_FLOOR = "room not found in floor";
    public static final String ROOM_NOT_FOUND_IN_BUILDING = "room not found in building";
    public static final String ROOM_NOT_FOUND = "room not found";

    public static final String DEPARTMENT_ID_EXIST = "deparment id exsit";
    public static final String BUILDING_EXIST = "building exist";
    public static final String ROOM_FEE_EXIST = "room fee exist";
    public static final String ROOM_TYPE_EXIST = "room type exist";
}
