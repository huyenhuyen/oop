package com.example.demo.constant;

import org.springframework.util.AntPathMatcher;

public class PermitRoutesDictionary {
    private String antPatterns;
    private AntPathMatcher antPathMatcher;

    public PermitRoutesDictionary() {
        this.antPathMatcher = new AntPathMatcher();
        this.antPatterns = "/api/login";
    }

    public String getAntPatterns() {
        return antPatterns;
    }

    public boolean contain(String path) {
            if (antPathMatcher.match(antPatterns, path)) {
                return true;
            }
        return false;
    }
}
