package com.example.demo;

import com.example.demo.constant.PermitRoutesDictionary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class DemoApplication {
	@Value("${password.encodingID}")
	private String passwordEncodingID;
    @Bean
    public PasswordEncoder passwordEncoder() {
        Map<String, PasswordEncoder> encoders = new HashMap<>();
        encoders.put("bcrypt", new BCryptPasswordEncoder());
        try {
            if (NoOpPasswordEncoder.getInstance() == null) {
                return passwordEncoder();
            }
            encoders.put("noop", NoOpPasswordEncoder.getInstance());
        }catch (Exception e){
            System.out.println("/n/n"+ e + "/n/n");
        }
        return new DelegatingPasswordEncoder(passwordEncodingID, encoders);

    }
    @Bean
    public PermitRoutesDictionary permitRoutesDictionary() {
        return new PermitRoutesDictionary();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


    public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}



}
