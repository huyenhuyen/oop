package com.example.demo.config;

import com.example.demo.Author.service.RoleAuthenticationService;
import com.example.demo.Bean.ApplicationRoutesDictionary;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Nhận jwt gửi lên, phục vụ cho Author(xác thực)
 */
public class JWTAuthorizationFilter extends OncePerRequestFilter {
    private RoleAuthenticationService userAuthenticationCheckingService;
    private ApplicationRoutesDictionary applicationRoutesDictionary;

    public JWTAuthorizationFilter(RoleAuthenticationService userAuthenticationCheckingService,
                                  ApplicationRoutesDictionary applicationRoutesDictionary) {
        this.userAuthenticationCheckingService = userAuthenticationCheckingService;
        this.applicationRoutesDictionary = applicationRoutesDictionary;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String method = request.getMethod();
        String uri = request.getRequestURI();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


        if (authentication == null) {
            filterChain.doFilter(request, response);
        } else {
            try {
                String route = applicationRoutesDictionary.getRoute(uri);
                if(route != null) {
                    userAuthenticationCheckingService.checkUserAuthorization(method, route,
                            (String) authentication.getPrincipal(),
                            authentication.getAuthorities());
                }
                filterChain.doFilter(request, response);
            } catch (Exception e) {
                responseErrorMessage(response, HttpStatus.FORBIDDEN.value(), "ROLE_IS_NOT_ALLOWED");
            }
        }
    }

    private void responseErrorMessage(HttpServletResponse response,
                                      int statusCode,
                                      String message) throws IOException {
        response.setStatus(statusCode);
        response.setHeader("Content-Type", "application/json; charset=UTF-8");
        ObjectMapper objectMapper = new ObjectMapper();
        response.getWriter().write("RESPONSE ERROR MESSAGE");
    }
}
