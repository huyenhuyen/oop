package com.example.demo.config;

import com.example.demo.Author.service.RoleAuthenticationService;
import com.example.demo.Bean.ApplicationRoutesDictionary;
import com.example.demo.constant.PermitRoutesDictionary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.savedrequest.RequestCacheAwareFilter;

@Configuration
@EnableResourceServer
public class ResourceConfig extends ResourceServerConfigurerAdapter {
    @Value("${security.oauth2.resource.id}")
    private String resourceID;
    // The DefaultTokenServices bean provided at the AuthorizationConfig
    @Autowired
    private DefaultTokenServices tokenService;
    // The TokenStore bean provided at the AuthorizationConfig
    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private RoleAuthenticationService roleAuthenticationService;
    @Autowired
    private ApplicationRoutesDictionary applicationRoutesDictionary;
    @Autowired
    private PermitRoutesDictionary permitRoutesDictionary;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers(permitRoutesDictionary.getAntPatterns())
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                // Add các filter vào ứng dụng của chúng ta,
                // thứ mà sẽ hứng các request để xử lý trước khi tới các xử lý trong controllers.
                .addFilterBefore(new JWTAuthorizationFilter(roleAuthenticationService, applicationRoutesDictionary),
                        RequestCacheAwareFilter.class);
    }

    // To allow the ResourceServerConfigurerAdapter to understand the token,
    // it must share the same characteristics with AuthorizationServerConfigurerAdapter.
    // So, we must wire it up the beans in the ResourceServerSecurityConfigurer.

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(resourceID)
                .tokenServices(tokenService)
                .tokenStore(tokenStore);
    }
}
