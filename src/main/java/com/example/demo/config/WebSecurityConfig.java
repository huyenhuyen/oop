package com.example.demo.config;


import com.example.demo.Author.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * configures a basic form based login page.
 * Furthermore it secures all OAuth endpoints exposed by the Auth Server.
 * For simplicity's sake an in memory store of users is also included.
 */

/**
 *  WebSecurityConfigurerAdapter class to customize the security framework to our needs.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private PasswordEncoder passwordEncoder;;
    @Autowired
    private UserService userService;

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    /**
     *  It allows configuring web-based security for specific http requests.
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and()
                .formLogin().disable() // disable form authentication
                .anonymous().disable() // disable anonymous user
                .httpBasic()
                .and()
                .authorizeRequests().
                anyRequest().authenticated(); // Các request còn lại đều cần được authenticated
    }
    //Tải dữ liệu người dùng cụ thể trong security Framework
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());

    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userService);
        //Mã hóa và xác nhận mật khẩu
        try {
            authProvider.setPasswordEncoder(passwordEncoder);
        }
        catch (Exception e){
            System.out.println(e);
        }
        return authProvider;
    }
}
