package com.example.demo.base.response;

import org.springframework.http.HttpStatus;

public class OkResponse<T> extends BaseResponse<T> {

    public OkResponse() {
        super(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());
    }

    public OkResponse(T data) {
        super(HttpStatus.OK, HttpStatus.OK.name(), data);
    }
}
