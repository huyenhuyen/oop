package com.example.demo.base.response;

import org.springframework.http.HttpStatus;

public class ForbiddenResponse extends BaseResponse {
    public ForbiddenResponse() {
        super(HttpStatus.FORBIDDEN, HttpStatus.FORBIDDEN.getReasonPhrase());
    }

    public ForbiddenResponse(String msg) {
        super(HttpStatus.FORBIDDEN, msg);
    }
}
