package com.example.demo.base.response;

import org.springframework.http.HttpStatus;

public class UnAuthorizationResponse extends BaseResponse {
    public UnAuthorizationResponse(String msg) {
        super(HttpStatus.UNAUTHORIZED, msg);
    }
}
