package com.example.demo.base.response;

import org.springframework.http.HttpStatus;

public class BadRequestResponse extends BaseResponse {
    public BadRequestResponse() {
        super(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase());
    }

    public BadRequestResponse(String msg) {
        super(HttpStatus.BAD_REQUEST, msg);
    }

    public <T> BadRequestResponse(String msg, T data) {
        super(HttpStatus.BAD_REQUEST, msg, data);
    }
}
