package com.example.demo.base.response;

import org.springframework.http.HttpStatus;

public class InternalServerErrorResponse extends BaseResponse {
    public InternalServerErrorResponse() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
}
