package com.example.demo.base.response;

import org.springframework.http.HttpStatus;

public class ConflictResponse extends BaseResponse {
    public ConflictResponse() {
        super(HttpStatus.CONFLICT, HttpStatus.CONFLICT.getReasonPhrase());
    }

    public ConflictResponse(String message) {
        super(HttpStatus.CONFLICT, message);
    }
}
