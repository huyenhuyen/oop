package com.example.demo.base.dto.error;


public class ErrorDTO {
//    @ApiModelProperty(notes = "Error target")
    private String target;
//    @ApiModelProperty(notes = "Requirement", position = 1)
    private String required;

    public ErrorDTO(String target, String required) {
        this.target = target;
        this.required = required;
    }

    public ErrorDTO() {
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }
}
