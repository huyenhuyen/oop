package com.example.demo.base.dto.ok;

import org.springframework.data.domain.Page;

import java.util.List;

//@ApiModel
public class PageDto<T> {
//    @ApiModelProperty(notes = "Index of page")
    private int pageIndex;
//    @ApiModelProperty(notes = "Size of page", position = 1)
    private int pageSize;
//    @ApiModelProperty(notes = "Total items number found", position = 2)
    private long totalItems;
//    @ApiModelProperty(notes = "List items of page", position = 3)
    private List<T> items;

    public PageDto() {
    }

    public PageDto(Page<T> page) {
        setPageIndex(page.getNumber());
        setPageSize(page.getSize());
        setTotalItems(page.getTotalElements());
        setItems(page.getContent());
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
