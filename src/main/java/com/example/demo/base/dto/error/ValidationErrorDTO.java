package com.example.demo.base.dto.error;

import java.util.ArrayList;
import java.util.List;

//@ApiModel
public class ValidationErrorDTO {
//    @ApiModelProperty(notes = "List of errors")
    private List<ErrorDTO> fieldErrors = new ArrayList<>();

    public ValidationErrorDTO() {
    }

    public List<ErrorDTO> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<ErrorDTO> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public void addError(String field, String msg) {
        fieldErrors.add(new ErrorDTO(field, msg));
    }
}
