package com.example.demo.base.controllers;

import com.example.demo.base.response.BaseResponse;
import com.example.demo.base.response.InternalServerErrorResponse;

import com.example.demo.constant.ResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(annotations = RestController.class)
public class GlobalExceptionHandler {
    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public BaseResponse handleInternalServerError(Exception e) {
        logger.error(ResponseMessage.INTERNAL_ERROR, e);
        return new InternalServerErrorResponse();
    }
}
