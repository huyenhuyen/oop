package com.example.demo.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ApplicationRoutesDictionary {
    private Path root;

    public ApplicationRoutesDictionary() {
        this.root = new Path(null);
    }

    public String addRoute(String route) {
        route = route.replaceAll("\\{\\w+\\}", "*");
        String[] routePaths;
        if (route.startsWith("/")) {
            routePaths = route.substring(1).split("/");
        } else {
            routePaths = route.split("/");
        }
        createChildPath(routePaths, 0, root);
        return route;
    }

    private void createChildPath(String[] routePaths, int index, Path parent) {
        String name = routePaths[index];
        Path pathFound = parent.getChildPathByName(name);
        if (pathFound == null) {
            pathFound = new Path(name);
            parent.addChildPath(pathFound);
        }
        index++;
        if (index < routePaths.length) {
            createChildPath(routePaths, index, pathFound);
        }
    }

    public String getRoute(String uri) {
        try {
            StringBuilder route = new StringBuilder();
            if (uri.startsWith("/")) {
                uri = uri.substring(1);
            }
            String uriParts[] = uri.split("/");
            constructRoute(uriParts, 0, root, route);
            return route.toString();
        } catch (Exception e) {
            return null;
        }
    }

    private void constructRoute(String[] uriParts, int index, Path parent, StringBuilder result) throws Exception {
        if (index < uriParts.length && !parent.isEnd()) {
            Path pathFound = parent.getChildPathByName(uriParts[index]);
            if(pathFound == null) {
                throw new Exception();
            } else {
                result.append("/").append(pathFound.name);
                constructRoute(uriParts, index + 1, pathFound, result);
            }
        }
    }

    private class Path {
        private String name;
        private Map<String, Path> childrenMap;


        public Path(String name) {
            this.name = name;
            this.childrenMap = new HashMap<>();
        }

        public void addChildPath(Path child) {
            childrenMap.put(child.name, child);
        }

        public boolean isEnd() {
            return childrenMap.size() == 0;
        }

        public Path getChildPathByName(String childName) {
            Path pathFound = childrenMap.get(childName);
            if(pathFound == null) {
                return childrenMap.get("*");
            }
            return pathFound;
        }
    }
}
