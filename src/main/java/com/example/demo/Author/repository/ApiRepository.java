package com.example.demo.Author.repository;

import com.example.demo.Author.models.Entity.Api;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpMethod;

import java.util.Set;

public interface ApiRepository extends JpaRepository<Api, Integer> {
    @Query("select a from Api a " +
            "left join fetch a.roles " +
            "where a.method = ?1 " +
            "and a.route = ?2")
    Api getApiWithRoles(HttpMethod method, String route);
}