package com.example.demo.Author.repository;

import com.example.demo.Author.models.Entity.Role;
import com.example.demo.Author.models.Entity.RoleType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findFirstById(Integer id);

    @Query("select u.role from User u")

    @Modifying
    @Transactional
    void deleteById(int id);
}
