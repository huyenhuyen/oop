package com.example.demo.Author.repository;

import com.example.demo.Author.models.Entity.Role;
import com.example.demo.Author.models.Entity.User;
import org.hibernate.sql.Select;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<User, String> {

//    User getUserByUsername(String username);

//    @Query("select u.username, u.role from User u where u.username = ?1")
    User findFirstByUsername(String username);

    @Query("select u.id from User u where u.username = ?1")
    String getIDWithUsername(String username);

    @Modifying
    @Transactional
    void deleteById(int id);
}
