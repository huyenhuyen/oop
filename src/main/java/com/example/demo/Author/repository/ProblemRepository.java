package com.example.demo.Author.repository;

import com.example.demo.Author.models.Entity.Problem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProblemRepository extends JpaRepository<Problem, Integer> {
    Problem findFirstByName(String name);
}
