package com.example.demo.Author.controller;

import com.example.demo.Author.models.Dto.LoginDto;
import com.example.demo.Author.service.LoginService;
import com.example.demo.base.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public BaseResponse login(@RequestHeader(value = "Authorization", defaultValue = "Basic Base64(clientID:secret)") String basicAuth,
                              @RequestBody @Valid LoginDto loginDto) {
        return loginService.login(basicAuth, loginDto);
    }
}
