package com.example.demo.Author.controller;

import com.example.demo.Author.models.Dto.ProblemDto;
import com.example.demo.Author.models.Dto.UserDto;
import com.example.demo.Author.models.Entity.Problem;
import com.example.demo.Author.repository.UserRepository;
import com.example.demo.Author.service.ProblemService;
import com.example.demo.Author.service.UserService;
import com.example.demo.base.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class MethodController {
    @Autowired
    UserService userService;

    @Autowired
    private ProblemService problemService;

    @GetMapping("/getProblem")
    public String getProblem(){
        return "Get success!";
    }

    @PostMapping("/createProblem")
    public String createProblem(@RequestBody @Valid ProblemDto problemDto){
        return problemService.createProblem(problemDto);
    }

    @PutMapping("/updateProblem/{id}")
    public String updateProblem(@PathVariable("id") int problemID, ProblemDto problemDto){
        return problemService.updateProblem(problemID, problemDto);
    }

    @DeleteMapping("/deleteProblem")
    public String deletePoblem(ProblemDto problemDto){
        return "delete susscess!";
    }
    //Update quyền
    @PostMapping("/updateRole")
    public String updateRole(@RequestBody @Valid UserDto userDto){
        return userService.updateRole(userDto);
    }


}
