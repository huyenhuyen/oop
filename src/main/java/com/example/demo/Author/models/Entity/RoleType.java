package com.example.demo.Author.models.Entity;

public enum RoleType {
    TEACHER, LEADER, STUDENT
}
