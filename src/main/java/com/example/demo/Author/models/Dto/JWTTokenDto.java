package com.example.demo.Author.models.Dto;

/**
 *  thông tin JWT
 */
public class JWTTokenDto {
    private String access_token;
    private String toke_type;
    private String refresh_token;
    private Long expires_in;
    private String scope;
    private String jti;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToke_type() {
        return toke_type;
    }

    public void setToke_type(String toke_type) {
        this.toke_type = toke_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public static enum RoleType {
        TEACHER, LEADER, STUDENT
    }
}
