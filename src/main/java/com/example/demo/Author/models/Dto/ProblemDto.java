package com.example.demo.Author.models.Dto;

/**
 * đối tượng chứa name và userID trong problem
 */
public class ProblemDto {
    private String name;
//    private String useID;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getUseID() {
//        return useID;
//    }
//
//    public void setUseID(String useID) {
//        this.useID = useID;
//    }
}
