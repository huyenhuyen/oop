package com.example.demo.Author.models.Dto;

public class UserTokenDto {
    private String id;
    private String accessToken;
    private String refreshToken;

    public UserTokenDto(String id, String accessToken, String refreshToken) {
        this.id = id;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshtoken() {
        return refreshToken;
    }

    public void setRefreshtoken(String refreshtoken) {
        this.refreshToken = refreshtoken;
    }
}
