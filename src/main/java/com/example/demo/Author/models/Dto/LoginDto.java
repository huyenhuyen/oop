package com.example.demo.Author.models.Dto;

import org.springframework.web.bind.annotation.RestController;

/**
 * lưu thông tin login (username, pass)
 */

public class LoginDto {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
