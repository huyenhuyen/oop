package com.example.demo.Author.service;

import com.example.demo.Author.models.Dto.JWTTokenDto;
import com.example.demo.Author.models.Dto.LoginDto;
import com.example.demo.Author.models.Dto.UserTokenDto;
import com.example.demo.Author.repository.UserRepository;
import com.example.demo.base.response.BadRequestResponse;
import com.example.demo.base.response.BaseResponse;
import com.example.demo.base.response.OkResponse;
import com.example.demo.base.response.UnAuthorizationResponse;
import com.example.demo.constant.ResponseMessage;
import com.example.demo.untils.HttpPostRequestBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class LoginService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RestTemplate restTemplate;

    @Value("${server.port}")
    private int serverPort;

    public BaseResponse login(String basicAuth, LoginDto loginDto) {
        LinkedMultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("grant_type", "password");
        body.add("username", loginDto.getUsername());
        body.add("password", loginDto.getPassword());

        try {
            JWTTokenDto tokenDto = new HttpPostRequestBuilder(restTemplate)
                    .withUrl("http://localhost:" + serverPort + "/oauth/token")
                    .setContentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .addToHeader("Authorization", basicAuth)
                    .setFormDataBody(body)
                    .execute(JWTTokenDto.class);
            return new OkResponse<>(
                    new UserTokenDto(userRepository.getIDWithUsername(loginDto.getUsername()), tokenDto.getAccess_token(), tokenDto.getRefresh_token()));
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    return new UnAuthorizationResponse(ResponseMessage.WRONG_CLIENT_ID_OR_SECRET);

                case BAD_REQUEST:
                    return new BadRequestResponse(ResponseMessage.WRONG_USERNAME_OR_PASSWORD);
            }
            throw e;
        }
    }
}
