package com.example.demo.Author.service;

import com.example.demo.Author.models.Dto.UserDto;
import com.example.demo.Author.models.Entity.Role;
import com.example.demo.Author.models.Entity.User;
import com.example.demo.Author.repository.RoleRepository;
import com.example.demo.Author.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Create a custom implementation of UserDetailsService to help
 * Spring Security loading user-specific data in the framework.
 *  Là userdetail - tải dữ liệu người dùng trong dtb
 */
@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;

    /**
     *searches the database for a record containing it,
     *  and (if found) returns an instance of User
     *  this process is executed outside this class,
     *  by the Spring Security framework.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findFirstByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(),
                getGrantedAuthorities(user));
    }

    /**
     * Lấy ra quyền được cấp
     * @param user
     * @return
     */
    public List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> result = new ArrayList<>(1);
        if(user.getRole() != null ) {
            result.add(new SimpleGrantedAuthority("" + user.getRole().getId()));
        }
        return result;
    }

    /**
     * update quyền
     * @param userDto tên và quyền muốn cài đặt
     * @return đã update quyền
     */
    public String updateRole(UserDto userDto){
        try {
            User user = userRepository.findFirstByUsername(userDto.getUsername());
            if(user == null){
                return  "not found username";
            }
            Role role = roleRepository.findFirstById(userDto.getRoleID());
            user.setRole(role);
            userRepository.save(user);
        }
        catch (Exception e){
            System.out.println("/n--------------------/n" + e + "/n------------------");
        }
        return "Đã thêm quyền cho " + userDto.getUsername();

    }
}
