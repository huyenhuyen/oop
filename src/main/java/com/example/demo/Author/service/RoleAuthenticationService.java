package com.example.demo.Author.service;

import com.example.demo.Author.models.Entity.Api;
import com.example.demo.Author.models.Entity.Role;
import com.example.demo.Author.models.Entity.RoleType;
import com.example.demo.Author.repository.ApiRepository;
import com.example.demo.Author.repository.RoleRepository;
import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;
import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class RoleAuthenticationService {
    @Value("${jwt-token.signing-key}")
    private String jwtSigningKey;

    @Autowired
    private ApiRepository apiRepository;
    @Autowired
    private RoleRepository roleRepository;


    public void checkUserAuthorization(String method, String route,
                                       String username,
                                       Collection<? extends GrantedAuthority> authorities) throws Exception {
        if (authorities.size() == 0) {
            throw new Exception(); //Không tìm thấy quyền
        }


        String roleID = authorities.iterator().next().getAuthority();
        //lay quyen
        Role userRole = roleRepository.findFirstById(Integer.parseInt(roleID));
        if (userRole.getType() == RoleType.TEACHER) {
            return;
        }

        Api api = apiRepository.getApiWithRoles(HttpMethod.valueOf(method), route);
        //api khác null và api.getRoles() != quyền
        if (api != null && !api.getRoles().contains(userRole)) {
            throw new Exception();
        }
    }
}
